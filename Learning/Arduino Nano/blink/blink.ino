int pinLED = 4;


void setup() {
  pinMode(pinLED,OUTPUT);
  digitalWrite(pinLED,LOW);
}

void loop() {
  digitalWrite(pinLED,LOW);
  delay(1000);
  digitalWrite(pinLED,HIGH);
  delay(1000);
}
