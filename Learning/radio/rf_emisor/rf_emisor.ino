#include <VirtualWire.h>
 
const int dataPin = 8;
const int buttonPin = 6;
const int ledPin = 7;
int buttonState = 0;    
 
void setup()
{
    Serial.begin(9600);     
    pinMode(buttonPin,INPUT);
    pinMode(ledPin,OUTPUT);
    digitalWrite(ledPin,LOW);
    vw_setup(2000);
    vw_set_tx_pin(dataPin);
}
 
void loop()
{
    char data[1];
    data[0] = 'a';
    buttonState = digitalRead(buttonPin);
    if (buttonState == HIGH) {
      digitalWrite(ledPin,HIGH);
      vw_send((uint8_t*)data,sizeof(data));
    } else {
      digitalWrite(ledPin,LOW);  
    }
    vw_wait_tx();
}
