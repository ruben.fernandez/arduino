#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <VirtualWire.h>
#include <Thread.h>
 
const int dataPin = 8;
const int buttonPin = 6;
const int ledPin = 7;
const int vrx = A1;
const int vry = A0;
const int led2Pin = 9;

int buttonState = 0;    
int vx = 0;
int vy = 0;

Thread threadStick = Thread();

void leerStick(){
    vx = map( analogRead(vrx), 0, 1023, 0, 100 );
    vy = map( analogRead(vry), 0, 1023, 0, 100 );
    Serial.print(vx);
    Serial.print(" --- ");
    Serial.println(vy);
}


void setup() {
    Serial.begin(9600);     
    pinMode(buttonPin,INPUT);
    pinMode(ledPin,OUTPUT);
    
    digitalWrite(ledPin,LOW);
    vw_setup(2000);
    vw_set_tx_pin(dataPin);
  
    threadStick.onRun(leerStick);
    threadStick.setInterval(0);
}
 
void loop() {
    char data[1];
    data[0] = 'a';
    buttonState = digitalRead(buttonPin);
    if (buttonState == HIGH) {
      digitalWrite(ledPin,HIGH);
      vw_send((uint8_t*)data,sizeof(data));
    } else {
      digitalWrite(ledPin,LOW);  
    }
    vw_wait_tx();


    

    if(threadStick.shouldRun())
      threadStick.run();
  


}
